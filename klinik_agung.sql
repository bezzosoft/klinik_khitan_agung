-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 02, 2017 at 12:49 AM
-- Server version: 5.7.17-0ubuntu0.16.10.1
-- PHP Version: 7.0.15-0ubuntu0.16.10.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinik_agung`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_galleries`
--

CREATE TABLE `category_galleries` (
  `id` int(11) NOT NULL,
  `name_category` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_galleries`
--

INSERT INTO `category_galleries` (`id`, `name_category`) VALUES
(1, 'promosi'),
(2, 'kondisi_ruangan');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title_content` varchar(45) DEFAULT NULL,
  `value_content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title_content`, `value_content`) VALUES
(1, 'Faidah Khitan menurut syariat dam medis', '<p style="margin-bottom: 0.14in; line-height: 115%;" align="justify"><span style="font-family: \'Times New Roman\', serif;"><span style="font-size: large;">Hasil penelitian di Sydney Medical School menemukan pria yang tidak sunat lebih beresiko terkena penyakit infeksi . Khitan dapat memberikan perlindungan sepanjang hidup dan menurunkan risiko penyakit saluran kemih</span></span></p>\r\n<p></p>\r\n<p style="margin-bottom: 0.14in; line-height: 115%;" align="justify"><span style="font-family: \'Times New Roman\', serif;"><span style="font-size: 16pt;">&nbsp;Menurut Syaikh Abdullah Nasih Ulwaan dalam buku kitab Tarbiyatul Aulaad Fiil Islam , khitan memiliki faedah sebagai syar atau ciri syariat Islam.</span></span></p>'),
(2, 'khitan', '<p style="margin-bottom: 0.14in; line-height: 115%;" align="justify"><span style="font-family: \'Times New Roman\', serif;"><span style="font-size: large;">Sejalan dengan perkembangan zaman dan perkembangan medis. Kalau dahulu khitan menggunakan bilah bambu , kemudian menggunakan metode dengan gunting dan perkakas.&nbsp;</span></span><span style="font-family: \'Times New Roman\', serif;"><span style="font-size: large;">Maka&nbsp;Klinik Khitan Agung memperhatikan hal tersebut. Klinik Khitan Agung memiliki beberapa metode khitan yaitu :</span></span></p>'),
(3, 'metode_khitan_konvensional', 'Di Klinik Khitan Agung metode ini mengacu kepada aturan atau standar medis , sehingga meningkatkan keberhasilan khitan / sunat. Hal yang dilakukan pada metode konvensional atau umum adalah :'),
(4, 'sub_metode_konvensional', '<ul>\r\n<li>Pembiusan Local</li>\r\n<li>Pembersihan penis</li>\r\n<li>Penggunaan pisau bedah / bisturi yang steril</li>\r\n<li>Tenaga dokter / medis yang professional</li>\r\n<li>Benang jahit yang bisa menyatu dengan jaringan sehingga tidak perlu untuk melepas benang jahit.</li>\r\n<li>Peralatan yang sekali pakai sehingga terhindar dari penyakit menular , seperti HIV dan lain-lain</li>\r\n</ul>'),
(5, 'title_metode_konvensional', 'Metode konvensional / Umum / Klasik'),
(6, 'title_metode_klamp', 'Metode Klamp'),
(7, 'metode_khitan_klamp', 'Alat klam terbuat dari plastic dan terdiri dari tabung dan pengunci. Pada prinsifnya cara kerja klamp adalah kulit yang akan dihilangkan dipotong saat itu juga. Prosedurnya : <br>'),
(8, 'sub_metode_klamp', '<ul>\r\n<li>Pembiusan local</li>\r\n<li>Pembersihan penis</li>\r\n<li>Kulit yang akan dihilangkan dilebarkan , kemudian di tahan dengan cara klamp</li>\r\n<li>Metode ini minimal rasa sakit, minimal perdarahan dan bisa langsung beraktifitas yang relative ringan, anak boleh langsung mandi dan boleh bersekolah</li>\r\n</ul>'),
(9, 'title_metode_clauter', 'Metode Electric Cauter'),
(10, 'metode_khitan_clauter', 'Metode ini menggunakan elemen yang dipanaskan dalam waktu yang sangat singkat. Metode ini memiliki kelebihan dalam hal mengatur perdarahan.'),
(11, 'khitan_include', 'Biaya tindakan oleh dokter'),
(12, 'khitan_include', 'Control sampai sembuh'),
(13, 'khitan_include', 'Celana Khitan'),
(14, 'khitan_include', 'Obat - obat untuk dirumah'),
(15, 'khitan_include', 'Peralatan pasca khitan / pasca lepas klam'),
(16, 'tentang_klinik', 'Klinik Khitan Agung merupakan klinik khitan yang berlokasi di Tanjung Priok - Jakarta Utara. Merupakan Komitmen bagi kami untuk memberikan pelayanan khitan terbaik. Hal ini didukung oleh metode khitan yang kami terapkan merupakan metode khitan terbaru serta fasilitas peralatan lengkap dan terkini. Kondisi dari Klinik Khitan Agung pun, sangat nyaman dan memiliki ruang tunggu yang sangat menarik bagi anak - anak.');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `source_gallery` varchar(45) DEFAULT NULL,
  `category_gallery_id` int(11) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `source_gallery`, `category_gallery_id`, `description`) VALUES
(1, 'ruang_tindakan', 'ruang_tindakan.jpg', 2, 'Ruang tindakan ini merupakan ruangan yang digunakan untuk proses khitan anak.'),
(2, 'ruang_tunggu', 'ruang_tunggu.jpg', 2, 'Ruang tunggu proses khitan anak. Untuk keluarga yang mengantar dapat menunggu di ruangan ini'),
(3, 'pintu_ruangan', 'pintu_depan.jpg', 2, 'Kondisi ruangan terlihat dari pintu depan klinik khitan agung.'),
(4, 'icon_klinik', 'icon.jpg', 1, 'Icon Klinik Khitan agung'),
(5, 'keunggulan', 'keunggulan.jpg', 1, 'Keunggulan dari klinik khitan agung dibanding dengan yang lain.');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title_setting` varchar(45) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title_setting`, `content`) VALUES
(1, 'phone', '021-4366694'),
(2, 'handphone', '082220202812'),
(3, 'facebook', 'https://www.facebook.com/klinikkhitanagung'),
(4, 'twitter', 'https://twitter.com/agungklinikid'),
(5, 'instagram', '@agungklinik'),
(6, 'email', 'klinikkhitanagung@gmail.com'),
(7, 'open', 'Senin - Minggu, Jam 08:00 - 20:00'),
(8, 'alamat', 'Klinik Khitan Agung <br>\r\nJl. yos sudarso lorong 104 timur No <br> \r\n71B pasar permai <br>\r\ntanjung priok-Jakarta Utara\r\nIndonesia');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Intan', 'klinikkhitanagung@gmail.com', '$2y$10$RpZfAQe.Gk9jU309tJmaA.pYMZ4mnMiy5sVIdaMx7KxV6DWdiVMkO', 'xgTF1b222wIwNgF5Jje3BVpYkY3jgvlJC6VqbHb7DcVoZUjzjEeOwgc4ZsSj', '2017-02-14 22:51:51', '2017-02-14 22:51:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_galleries`
--
ALTER TABLE `category_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_gallery_category_gallery_idx` (`category_gallery_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_galleries`
--
ALTER TABLE `category_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `fk_gallery_category_gallery` FOREIGN KEY (`category_gallery_id`) REFERENCES `category_galleries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
