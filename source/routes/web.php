<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');


Route::get('/home', 'HomeController@index');

Route::get('/detail-gallery/{id}', 'FrontendController@showGallery');

Route::get('/gallery', 'GalleryController@index');
Route::get('/tambah-gallery', 'GalleryController@create');
Route::post('/post-gallery', 'GalleryController@store');
Route::get('/edit-gallery/{id}', 'GalleryController@edit');
Route::put('/put-gallery/{id}', 'GalleryController@update');
Route::delete('/delete-gallery/{id}', 'GalleryController@destroy');

Route::get('/settings', 'SettingsController@index');
Route::get('/tambah-setting', 'SettingsController@create');
Route::post('/post-setting', 'SettingsController@store');
Route::get('/edit-setting/{id}', 'SettingsController@edit');
Route::put('/put-setting/{id}', 'SettingsController@update');
Route::delete('/delete-setting/{id}', 'SettingsController@destroy');

Route::get('/konten', 'ContentController@index');
Route::get('/edit-konten/{id}', 'ContentController@edit');
Route::get('/tambah-konten', 'ContentController@create');
Route::put('/put-konten/{id}', 'ContentController@update');
Route::delete('/delete-konten/{id}', 'ContentController@destroy');
Route::post('/post-konten', 'ContentController@store');

Route::get('/kategori-gallery', 'CategoryGalleryController@index');
Route::get('/edit-kategori-gallery/{id}', 'CategoryGalleryController@edit');
Route::get('/tambah-kategori-gallery', 'CategoryGalleryController@create');
Route::put('/put-kategori-gallery/{id}', 'CategoryGalleryController@update');
Route::delete('/delete-kategori-gallery/{id}', 'CategoryGalleryController@destroy');
Route::post('/post-kategori-gallery', 'CategoryGalleryController@store');

Auth::routes();


