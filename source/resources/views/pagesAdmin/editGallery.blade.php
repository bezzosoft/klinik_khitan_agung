@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Foto</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("put-gallery/{$gallery->id}") }}">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}
            
            <div class="form-group">
                <img src="{{ asset("img/backend/gallery/$gallery->source_gallery") }}" alt="" class="img img-responsive">
            </div>
            <div class="form-group">
                <input type="text" name="name" class="form-control" value="{{ $gallery->name }}">
            </div>
            <div class="form-group">
                <label for="type">Tipe Foto</label>
                <select name="category_gallery_id" id="" class="form-control">
                    <option value="0">Pilih</option>
                    @foreach($category_galleries as $category_gallery)
                        <option value="{{ $category_gallery->id }}">{{ $category_gallery->name_category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control">
                    {{ $gallery->description }}
                </textarea>
            </div>

            <button class="btn btn-success">Edit</button>
        </form>
        <br>
    </div>
</div>
@endsection