@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Setting</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("put-setting/{$setting->id}") }}">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}
            <div class="form-group">
                <label>Nama Setting</label>
                <input class="form-control" name="title_setting" value="{{ $setting->title_setting }}">
            </div>
            <div class="form-group">
                <label>Deskripsi Konten</label>
                <textarea class="form-control" rows="4" name="content">{{ $setting->content }}</textarea>
            </div>

            <button class="btn btn-success">Edit</button>
        </form>
    </div>
</div>
@endsection