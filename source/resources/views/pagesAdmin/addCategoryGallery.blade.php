@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tambah Kategori Gallery</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("post-kategori-gallery") }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label>Nama Kategori</label>
                <input class="form-control" name="name_category">
            </div>

            <button class="btn btn-success">Tambah</button>
        </form>
    </div>
</div>
@endsection