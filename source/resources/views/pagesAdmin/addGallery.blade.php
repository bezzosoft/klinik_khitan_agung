@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tambah Gallery</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("post-gallery") }}" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" name="name">
            </div>
            <div class="form-group">
                <label>Source</label>
                <input type="file" name="source_gallery" class="form-control">
            </div>
            <div class="form-group">
                <label>Kategori</label>
                <select name="category_gallery_id" id="" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name_category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control"></textarea>
            </div>

            <button class="btn btn-success">Tambah</button>
        </form>
        <br>
    </div>
</div>
@endsection
