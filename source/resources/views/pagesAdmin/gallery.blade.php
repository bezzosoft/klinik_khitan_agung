@extends('pagesAdmin.admintemplatemaster')

@section('styles')
    <!-- DataTables CSS -->
    <link href="{{ asset("assets_backend/datatables-plugins/dataTables.bootstrap.css") }}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset("assets_backend/datatables-responsive/dataTables.responsive.css") }}" rel="stylesheet">
@endsection

@section('navbar')
    @parent
@endsection

@section('gallery')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Gallery</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ url("tambah-gallery") }}" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Tambah</a>
        </div>
    </div>
    <br>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Data Gallery
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Category</th>
                            <th>Resource</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($galleries as $gallery)
                            <tr class="odd gradeX">
                                <td>{{ $no }}</td>
                                <td>{{ $gallery->name }}</td>
                                <td>
                                    @php
                                        $category = new \App\Models\CategoryGallery();
                                        $category_gallery = $category->show($gallery->category_gallery_id);
                                    @endphp
                                    {{ $category_gallery->name_category }}
                                </td>
                                <td><img src="{{ asset("img/backend/gallery/$gallery->source_gallery") }}" alt=""
                                         class="img img-responsive"></td>
                                <td>
                                    <a href="{{ url("edit-gallery/{$gallery->id}") }}" class="btn btn-warning">Edit</a>
                                    <form action="{{ url("delete-gallery/{$gallery->id}") }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button class="btn btn-primary">Delete</button>
                                    </form>
                                </td>
                            </tr>

                            @php($no++)
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection
@section('scripts')
    <!-- DataTables JavaScript -->
    <script src="{{ asset("assets_backend/datatables/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("assets_backend/datatables-plugins/dataTables.bootstrap.min.js") }}"></script>
    <script src="{{ asset("assets_backend/datatables-responsive/dataTables.responsive.js") }}"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection