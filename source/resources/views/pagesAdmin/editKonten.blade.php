@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Konten</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("put-konten/{$content->id}") }}">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}
            <div class="form-group">
                <label>Judul Konten</label>
                <input class="form-control" name="title_content" value="{{ $content->title_content }}">
            </div>
            <div class="form-group">
                <label>Deskripsi Konten</label>
                <textarea class="form-control" rows="4" name="value_content">{{ $content->value_content }}</textarea>
            </div>

            <button class="btn btn-success">Edit</button>
        </form>
    </div>
</div>
@endsection