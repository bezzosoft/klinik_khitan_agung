@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tambah Setting</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("post-setting") }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label>Nama Setting</label>
                <input class="form-control" name="title_setting"">
            </div>

            <div class="form-group">
                <label>Deskripsi Setting</label>
                <textarea class="form-control" rows="4" name="content"></textarea>
            </div>

            <button class="btn btn-success">Tambah</button>
        </form>
    </div>
</div>
@endsection