@extends('pagesAdmin.admintemplatemaster')

@section('navbar')
    @parent
@endsection

@section('gallery')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Kategori Gallery</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <form role="form" method="post" action="{{ url("put-kategori-gallery/{$kategori_gallery->id}") }}">
            <input type="hidden" name="_method" value="PUT">

            {{ csrf_field() }}
            <div class="form-group">
                <label>Judul Konten</label>
                <input class="form-control" name="name_category" value="{{ $kategori_gallery->name_category }}">
            </div>

            <button class="btn btn-success">Edit</button>
        </form>
    </div>
</div>
@endsection