<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" gallery="IE=edge">
    <meta name="viewport" gallery="width=device-width, initial-scale=1">
    <meta name="description" gallery="">
    <meta name="author" gallery="">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset("assets_backend/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">

    <!-- MetisMenu CSS -->
    <link href="{{ asset("assets_backend/metisMenu/metisMenu.min.css") }}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{ asset("css/backend/admins.css") }}" rel="stylesheet" type="text/css">

    <!-- Morris Charts CSS -->
    <link href="{{ asset("assets_backend/morrisjs/morris.css") }}" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="{{ asset("assets_backend/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css">

    @yield('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">

    @section('navbar')
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home">KLINIK KHITAN AGUNG</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-task">
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                               <i class="fa fa-sign-out fa-fw"></i> Logout</a>
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url("kategori-gallery") }}"><i class="fa fa-server fa-fw"></i>
                                Kategori Konten</a>
                        </li>
                        <li>
                            <a href="{{ url("gallery") }}"><i class="fa fa-picture-o fa-fw"></i> Gallery</a>
                        </li>
                        <li>
                            <a href="{{ url("konten") }}"><i class="fa fa-file-text-o fa-fw"></i> Konten</a>
                        </li>
                        <li>
                            <a href="{{ url("settings") }}"><i class="fa fa-wrench fa-fw"></i> Settings</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        @show

        <div id="page-wrapper">
            @yield('gallery')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset("assets_backend/jquery/jquery.min.js") }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset("assets_backend/bootstrap/js/bootstrap.min.js") }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset("assets_backend/metisMenu/metisMenu.min.js") }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset("assets_backend/raphael/raphael.min.js") }}"></script>
    <script src="{{ asset("assets_backend/morrisjs/morris.min.js") }}"></script>
    <script src="../data/morris-data.js"></script>

    @yield('scripts')

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset("js/backend/sb-admin-2.js") }}"></script>

</body>

</html>
