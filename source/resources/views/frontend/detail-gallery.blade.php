<div class="cbp-l-member-img">
    <img src="{{ asset("img/frontend/team/1.jpg") }}" alt="">
</div>
<div class="cbp-l-member-info">
    <div class="cbp-l-member-name">{{ $gallery->name }}</div>
    <div class="cbp-l-member-position">
        @php
            $category_gallery = new \App\Models\CategoryGallery();
            $category = $category_gallery->show($gallery->category_gallery_id);
        @endphp
        {{ $category->name_category }}
    </div>
    <div class="cbp-l-member-desc">
        {{ $gallery->description }}
    </div>
</div>
