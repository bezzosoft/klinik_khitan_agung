<?php

namespace App\Http\Controllers;

use App\Models\CategoryGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryGalleryController extends Controller
{
    private $categoryGallery;

    public function __construct()
    {
        $this->categoryGallery = new CategoryGallery();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["category_galleries"] = $this->categoryGallery->lov();
        return view("pagesAdmin/categoryGallery", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pagesAdmin/addCategoryGallery");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_gallery = new CategoryGallery();
        $category_gallery->name_category = $request->input("name_category");

        $category_gallery->save();
        return Redirect::to("kategori-gallery");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["kategori_gallery"] = $this->categoryGallery->show($id);
        return view("pagesAdmin/editCategoryGallery", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category_gallery = $this->categoryGallery->show($id);
        $category_gallery->name_category = $request->input("name_category");

        $category_gallery->save();
        return Redirect::to("kategori-gallery");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category_gallery = $this->categoryGallery->show($id);
        $category_gallery->delete();

        return Redirect::to("kategori-gallery");
    }
}
