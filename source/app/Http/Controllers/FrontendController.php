<?php

namespace App\Http\Controllers;

use App\Models\CategoryGallery;
use App\Models\Content;
use App\Models\Gallery;
use App\Models\Setting;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    private $category_gallery;
    private $gallery;
    private $setting;
    private $content;

    public function __construct()
    {
        $this->category_gallery = new CategoryGallery();
        $this->gallery = new Gallery();
        $this->setting = new Setting();
        $this->content = new Content();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["category_galleries"] = $this->category_gallery->lov();
        $data["galleries"] = $this->gallery->lov();
        $data["phone"] = $this->setting->showFind("phone");
        $data["handphone"] = $this->setting->showFind("handphone");
        $data["email"] = $this->setting->showFind("email");
        $data["open"] = $this->setting->showFind("open");
        $data["alamat"] = $this->setting->showFind("alamat");
        $data["facebook"] = $this->setting->showFind("facebook");
        $data["twitter"] = $this->setting->showFind("twitter");
        $data["khitan"] = $this->content->showFindFirst("khitan");
        $data["khitan_includes"] = $this->content->showFind("khitan_include");

        //konvensional
        $data["title_metode_konvensional"] = $this->content->showFindFirst("title_metode_konvensional");
        $data["konvensional"] = $this->content->showFindFirst("metode_khitan_konvensional");
        $data["sub_metode_konvensional"] = $this->content->showFindFirst("sub_metode_konvensional");

        //klamp
        $data["title_metode_klamp"] = $this->content->showFindFirst("title_metode_klamp");
        $data["klamp"] = $this->content->showFindFirst("metode_khitan_klamp");
        $data["sub_metode_klamp"] = $this->content->showFindFirst("sub_metode_klamp");

        //clauter
        $data["title_metode_clauter"] = $this->content->showFindFirst("title_metode_clauter");
        $data["clauter"] = $this->content->showFindFirst("metode_khitan_clauter");
        $data["sub_metode_clauter"] = $this->content->showFindFirst("sub_metode_clauter");

        $data["tentang_klinik"] = $this->content->showFindFirst("tentang_klinik");
        return view("index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showGallery($id){
        $data["gallery"] = $this->gallery->show($id);
        return view("frontend/detail-gallery", $data);
    }
}
