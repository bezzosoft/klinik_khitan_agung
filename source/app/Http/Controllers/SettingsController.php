<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\TypeSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SettingsController extends Controller
{
    private $settings;

    public function __construct()
    {
        $this->settings = new Setting();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["settings"] = $this->settings->lov();
        return view("pagesAdmin/settings", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pagesAdmin/addSettings");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $setting = new Setting();
        $setting->title_setting = $request->input("title_setting");
        $setting->content = $request->input("content");
        $setting->save();

        return Redirect::to("settings");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["setting"] = $this->settings->show($id);
        return view("pagesAdmin/editSettings", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = $this->settings->show($id);
        $setting->title_setting = $request->input("title_setting");
        $setting->content = $request->input("content");

        $setting->save();

        return Redirect::to("settings");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = $this->settings->show($id);
        $setting->delete();

        return Redirect::to("settings");
    }
}
