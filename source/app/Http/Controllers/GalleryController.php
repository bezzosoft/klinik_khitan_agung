<?php

namespace App\Http\Controllers;

use App\Models\CategoryGallery;
use App\Models\Gallery;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use League\Flysystem\Exception;

class GalleryController extends Controller
{
    private $gallery;
    private $category_gallery;

    public function __construct()
    {
        $this->gallery = new Gallery();
        $this->category_gallery = new CategoryGallery();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["galleries"] = $this->gallery->lov();
        return view('pagesAdmin/gallery', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["categories"] = $this->category_gallery->lov();
        return view("pagesAdmin/addGallery", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new Gallery();
        $gallery->name = $request->input("name");
        $gallery->category_gallery_id = $request->input("category_gallery_id");
        $gallery->description = $request->input("description");
        if ($request->hasFile("source_gallery")){
            if ($request->file("source_gallery")){
                $gallery->source_gallery = $request->file("source_gallery")->getClientOriginalName();
                $request->file("source_gallery")->move("img/backend/gallery", $gallery->source_gallery);
                $gallery->save();
                return Redirect::to("gallery");
            }
        }
        else{
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["category_galleries"] = $this->category_gallery->lov();
        $data["gallery"] = $this->gallery->show($id);
        return view("pagesAdmin/editGallery", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = $this->gallery->show($id);
        $category = $request->input("category_gallery_id");
        if ($category != 0){
            $gallery->category_gallery_id = $category;
        }
        $gallery->name = $request->input("name");
        $gallery->description = $request->input("description");

        $gallery->save();

        return Redirect::to("gallery");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = $this->gallery->show($id);
        unlink("img/backend/gallery/$gallery->source_gallery");
        $gallery->delete();
        return Redirect::to("gallery");
    }
}
