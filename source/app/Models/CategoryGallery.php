<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryGallery extends Model
{
    public $timestamps = false;

    public function lov(){
        return CategoryGallery::all();
    }

    public function show($id){
        return CategoryGallery::find($id);
    }

}
