<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public $timestamps = false;

    public function lov(){
        return Content::all();
    }

    public function show($id){
        return Content::find($id);
    }

    public function showFindFirst($title){
        return Content::where("title_content", $title)
                ->first();
    }

    public function showFind($title){
        return Content::where("title_content", $title)->get();
    }
}
