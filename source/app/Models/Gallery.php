<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public $timestamps = false;

    public function lov(){
        return Gallery::all();
    }

    public function show($id){
        return Gallery::find($id);
    }
}
