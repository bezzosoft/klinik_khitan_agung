<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;

    public function lov(){
        return Setting::all();

    }

    public function show($id){
        return Setting::find($id);
    }

    public function showFind($title){
        return Setting::where("title_setting", $title)
            ->first();
    }
}
