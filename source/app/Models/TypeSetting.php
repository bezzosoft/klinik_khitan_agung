<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeSetting extends Model
{
    public $timestamps = false;

    public function lov(){
        return TypeSetting::all();
    }

    public function show($id){
        return TypeSetting::find($id);
    }
}
