<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Klinik Agung</title>

    <!-- css -->
    <link href="<?php echo e(asset("css/frontend/bootstrap.min.css")); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset("assets_frontend/vendor/font-awesome/css/font-awesome.min.css")); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset("assets_frontend/plugins/cubeportfolio/css/cubeportfolio.min.css")); ?>">
    <link href="<?php echo e(asset("css/frontend/nivo-lightbox.css")); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset("css/frontend/nivo-lightbox-theme/default/default.css")); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset("css/frontend/owl.carousel.css")); ?>" rel="stylesheet" media="screen" />
    <link href="<?php echo e(asset("css/frontend/owl.theme.css")); ?>" rel="stylesheet" media="screen" />
    <link href="<?php echo e(asset("css/frontend/animate.css")); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset("css/frontend/style.css")); ?>" rel="stylesheet">

    <!-- boxed bg -->
    <link id="bodybg" href="<?php echo e(asset("css/frontend/bodybg/bg1.css")); ?>" rel="stylesheet" type="text/css" />
    <!-- template skin -->
    <link id="t-colors" href="<?php echo e(asset("css/frontend/color/default.css")); ?>" rel="stylesheet">

    <!-- =======================================================
        Theme Name: Medicio
        Theme URL: https://bootstrapmade.com/medicio-free-bootstrap-theme/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


<div id="wrapper">

    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="top-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <p class="bold text-left"><?php echo e($open->content); ?></p>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <p class="bold text-right">Call us now <?php echo e($phone->content); ?> | <?php echo e($handphone->content); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container navigation">

            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="<?php echo e(asset("img/frontend/logo.png")); ?>" alt="" width="150" height="40" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other gallery for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#intro">Beranda</a></li>
                    <li><a href="#facilities">Fasilitas</a></li>
                    <li><a href="#service">Harga</a></li>
                    <li><a href="#doctor">Gallery</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Section: intro -->
    <section id="intro" class="intro-content">
        <div class="intro-gallery">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
                            <h2 class="h-ultra">Klinik Khitan Agung</h2>
                        </div>
                        <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
                            <h4 class="h-light">
                                <?php echo $khitan->value_content; ?>

                            </h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="well well-trans">
                            <div class="wow fadeInRight" data-wow-delay="0.1s">

                                <ul class="lead-list">
                                    <li><i class="fa fa-check fa-2x icon-success"></i>
                                        <strong><?php echo $title_metode_klamp->value_content; ?></strong>
                                        <span class="list">
                                            <?php echo $klamp->value_content; ?>

                                            <?php if($sub_metode_klamp != null): ?>
                                                <?php echo $sub_metode_klamp->value_content; ?>

                                            <?php endif; ?>
                                        </span></li>
                                    <li><i class="fa fa-check fa-2x icon-success"></i>
                                        <strong><?php echo $title_metode_clauter->value_content; ?></strong>
                                        <span class="list">
                                            <?php echo $clauter->value_content; ?>

                                            <?php if($sub_metode_clauter != null): ?>
                                                <?php echo $sub_metode_clauter->value_content; ?>

                                            <?php endif; ?>
                                        </span></li>
                                    <li><i class="fa fa-check fa-2x icon-success"></i>
                                        <strong><?php echo $title_metode_konvensional->value_content; ?></strong>
                                        <span class="list">
                                            <?php echo $konvensional->value_content; ?>

                                            <?php if($sub_metode_konvensional != null): ?>
                                                <?php echo $sub_metode_konvensional->value_content; ?>

                                            <?php endif; ?>
                                        </span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /Section: intro -->

    <!-- Section: boxes -->
    <section id="facilities" class="home-section paddingtop-80">
        <div class="container">
            <br><br>
            <div class="row">
                <div align="center" class="wow fadeInDown">
                    <h2 class="h-ultra">
                        Fasilitas
                    </h2>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box text-center">

                                <i class="fa fa-check fa-3x circled bg-skin"></i>
                                <h4 class="h-bold">Tempat bermain untuk anak</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box text-center">

                                <i class="fa fa-check fa-3x circled bg-skin"></i>
                                <h4 class="h-bold">Ruang tindakan</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box text-center">
                                <i class="fa fa-check fa-3x circled bg-skin"></i>
                                <h4 class="h-bold">Ruang tunggu yang nyaman</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box text-center">

                                <i class="fa fa-check fa-3x circled bg-skin"></i>
                                <h4 class="h-bold">Wi-Fi Gratis</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /Section: boxes -->

    <!-- Section: services -->
    <section id="service" class="home-section nopadding paddingtop-60">

        <div class="container">
            <br><br><br>
            <div class="row">
                <div class="col-md-12 col-sm12 wow fadeInDown" align="center">
                    <h2 class="h-ultra">Harga</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm12 wow fadeInDown" align="center">
                    <h5>
                        Harga klinik khitan agung sangat terjangkau Rp.1,200,000 sudah termasuk
                    </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="wow fadeInUp" data-wow-delay="0.2s">
                        <img src="<?php echo e(asset("img/frontend/dummy/img-1.jpg")); ?>" class="img-responsive" alt="" />
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php $__currentLoopData = $khitan_includes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $khitan_include): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="wow fadeInRight" data-wow-delay="0.1s">
                                    <div class="service-box">
                                        <div class="service-icon">
                                            <span class="fa fa-plus-square fa-3x"></span>
                                        </div>
                                        <div class="service-desc">
                                            <h6 class="h-light"><?php echo e($khitan_include->value_content); ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Section: services -->


    <!-- Section: team -->
    <section id="doctor" class="home-section bg-gray paddingbot-60">
        <div class="container marginbot-50">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow fadeInDown" data-wow-delay="0.1s">
                        <div class="section-heading text-center">
                            <h2 class="h-bold">Gallery</h2>
                            <p>Gallery kondisi Klinik Khitan Agung</p>
                        </div>
                    </div>
                    <div class="divider-short"></div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div id="filters-container" class="cbp-l-filters-alignLeft">
                        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">All
                            (<div class="cbp-filter-counter"></div>)
                        </div>
                        <?php $__currentLoopData = $category_galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div data-filter=".<?php echo e($category_gallery->id); ?>" class="cbp-filter-item">
                                <?php echo e($category_gallery->name_category); ?>

                                (<div class="cbp-filter-counter"></div>)
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <div id="grid-container" class="cbp-l-grid-team">
                        <ul>
                            <?php $__currentLoopData = $galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="cbp-item <?php echo e($gallery->category_gallery_id); ?>">
                                    <a href="<?php echo e(url("detail-gallery/{$gallery->id}")); ?>" class="cbp-caption cbp-singlePage">
                                        <div class="cbp-caption-defaultWrap">
                                            <img src="<?php echo e(asset("img/backend/gallery/$gallery->source_gallery")); ?>" alt="" width="100%">
                                        </div>
                                        <div class="cbp-caption-activeWrap">
                                            <div class="cbp-l-caption-alignCenter">
                                                <div class="cbp-l-caption-body">
                                                    <div class="cbp-l-caption-text">VIEW PROFILE</div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?php echo e(url("detail-gallery")); ?>" class="cbp-singlePage cbp-l-grid-team-name">
                                        <?php echo e($gallery->name); ?>

                                    </a>
                                    <div class="cbp-l-grid-team-position">
                                        <?php 
                                            $category_gallery = new \App\Models\CategoryGallery();
                                            $category = $category_gallery->show($gallery->category_gallery_id);
                                         ?>
                                        <?php echo e($category->name_category); ?>

                                    </div>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /Section: team -->

    <footer>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="wow fadeInDown" data-wow-delay="0.1s">
                        <div class="widget">
                            <h5>Tentang Klinik Khitan Agung</h5>
                            <p align="justify">
                                <?php echo $tentang_klinik->value_content; ?>

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="wow fadeInDown" data-wow-delay="0.1s">
                        <div class="widget">
                            <h5>Klinik Khitan Agung</h5>
                            <ul>
                                <li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
								</span> <?php echo $open->content; ?>

                                </li>
                                <li>
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                                    </span> <?php echo $phone->content; ?>

                                </li>
                                <li>
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                                    </span> <?php echo $handphone->content; ?>

                                </li>
                                <li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span> <?php echo $email->content; ?>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="wow fadeInDown" data-wow-delay="0.1s">
                        <div class="widget">
                            <h5>Alamat</h5>
                            <p><?php echo $alamat->content; ?></p>

                        </div>
                    </div>
                    <div class="wow fadeInDown" data-wow-delay="0.1s">
                        <div class="widget">
                            <h5>Follow us</h5>
                            <ul class="company-social">
                                <li class="social-facebook"><a target="_blank" href="<?php echo e($facebook->content); ?>"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-twitter"><a target="_blank" href="<?php echo e($twitter->content); ?>"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="wow fadeInLeft" data-wow-delay="0.1s">
                            <div class="text-left">
                                <p>&copy;Copyright - Medicio Theme. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="wow fadeInRight" data-wow-delay="0.1s">
                            <div class="text-right">
                                <div class="credits">
                                    <!--
                                        All the links in the footer should remain intact.
                                        You can delete the links only if you purchased the pro version.
                                        Licensing information: https://bootstrapmade.com/license/
                                        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Medicio
                                    -->
                                    <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

<!-- Core JavaScript Files -->
<script src="<?php echo e(asset("js/frontend/jquery.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/bootstrap.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/jquery.easing.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/wow.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/jquery.scrollTo.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/jquery.appear.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/stellar.js")); ?>"></script>
<script src="<?php echo e(asset("assets_frontend/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/owl.carousel.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/nivo-lightbox.min.js")); ?>"></script>
<script src="<?php echo e(asset("js/frontend/custom.js")); ?>"></script>

</body>

</html>